package li.ues.topfriends.system

import java.io.Serializable
import java.util.ArrayList

/**
 * Created by schleumer on 12/01/16.
 */
class ImageMeta(threads: List<Thread>) : Serializable {

    var Threads: MutableList<Thread> = ArrayList()
    var RemovedThreads: List<Thread> = ArrayList()

    init {
//        Collections.sort(threads, object : Ordering<Thread>() {
//            override fun compare(left: Thread?, right: Thread?): Int {
//                return Longs.compare(left!!.MessageCount!!, right!!.MessageCount!!)
//            }
//        }.reverse())

        this.Threads = ArrayList()
        this.Threads.addAll(threads.sortedBy { -it.MessageCount })

        this.RemovedThreads = ArrayList()

    }
}
